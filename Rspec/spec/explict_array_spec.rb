RSpec.describe Array do
  subject(:sally) do
    %w[dog cat]
  end

  it 'has two key-value pairs' do
    expect(subject.length).to eq(2)
    subject.pop
    expect(sally.length).to eq(1)
  end

  it 'has two key-value pairs confirm' do
    expect(sally).to eq( %w[dog cat] )
  end
end
