RSpec.describe 'shorthand syntax' do
  subject { 5 }

  context 'with classic synatx' do
    it 'equal to 5' do
      expect(subject).to eq(5)
    end
  end

  context 'with one-liner synatx' do
    it { is_expected.to eq(5) }
  end
end



# If I could chime in and hopefully say something that hasn't yet been said... the only real difference(s) I know of are:

# subject will automatically be assigned as an instance of the class being tested.

# It allows for the use of "one liner" tests:

# context "when something" do
#   subject(:car) { FactoryGirl.create(:car) }

#   it { is_expected.to_not be_running }
# end
