class Card
  attr_accessor :type, :rank

  def initialize(type, rank)
    @type = type
    @rank = rank
  end
end

# RSpec.describe 'Card' do
#   it 'has a type' do
#     # assertion or expectation
#     card = Card.new('Ace of spades')
#     expect(card.type).to eq('Ace of spades')
#     #    expect(1 + 1).to(eq(2))
#   end
# end

# RSpec.describe Card do
#   it 'has a type' do
#     card = Card.new('Ace', 'spades')
#     expect(card.rank).to eq('Ace')
#     expect(card.suit).to eq('spades')
#   end
# end

# RSpec.describe Card do
#   before do
#     @card = Card.new('Ace', 'spades')
#   end
#   it 'has a type' do
#     expect(@card.type).to eq('Ace')
#   end

#   it 'has a rank' do
#     expect(@card.rank).to eq('spades')
#   end
# end

# RSpec.describe Card do
#   # def card
#   #     card = Card.new('Ace', 'spades')
#   # end
#   let(:card) { Card.new('Ace', 'spades') }
# #   let(:x) { 1 + 1 }
# #   let(:y) { x + 10 }

#   it 'has a type and that i can modify the description' do
#     # x ..gives us 2
#     expect(card.type).to eq('Ace')
#     card.type = 'queen'
#     expect(card.type).to eq('queen')
#   end 

#   it 'has a type' do
#     expect(card.type).to eq('Ace')
#   end

#   it 'has a rank' do
#     expect(card.rank).to eq('spades')
#   end
# end


RSpec.describe Card do
    let(:card) { Card.new('Ace', 'Spades') }

    it 'has a rank and that rank can change' do
      expect(card.type).to eq('Ace')
      card.type = 'Queen'
      expect(card.type).to eq('Queen')
    end
  
  #custom error message
    it 'has a custom error message' do
      card.rank = 'Nonsense'
      comparison = 'Spades'
      expect(card.rank).to eq(comparison), "Hey, I expected #{comparison} but I got #{card.rank} instead!"
    end
  end