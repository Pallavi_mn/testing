class ProgrammingLanguage
  attr_reader :name

  def initialize(name = 'Ruby')
    @name = name
  end
end

RSpec.describe ProgrammingLanguage do 
    let(:language) { ProgrammingLanguage.new('python') }
    
    it 'should store the name of the language' do 
        expect(language.name).to eq('python')
    end

    context 'with no initialize' do
        let(:language) { ProgrammingLanguage.new } #over writing let

        it 'defalut to ruby' do 
            expect(language.name).to eq('Ruby')
        end
    end
end
