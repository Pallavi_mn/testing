RSpec.shared_context 'common' do
  before do
    @foods = []
  end

  def some_helper_method
    5
  end

  let(:some_variables) { [1, 2, 3] }
end

RSpec.describe 'first example group' do
  include_context 'common'

  it 'can use outside instance variables' do
    expect(@foods.length).to eq(0)
    @foods << 'apple'
    expect(@foods.length).to eq(1)
  end

  it 'can reuse instance variables across diffrent examples' do
    expect(@foods.length).to eq(0)
  end

  it 'can use shared helper methods' do
    expect(some_helper_method).to eq(5)
  end
end

RSpec.describe 'Second example in different file' do
  include_context 'common'

  it 'can use shared let variables' do
    expect(some_variables).to eq([1, 2, 3])
  end
end



# The include_examples method injects predefined examples into an example group.

# The include_context method injects context (i.e. before blocks, instance variables, helper methods, let variables) into an example group.

# Both remove duplication across tests.
