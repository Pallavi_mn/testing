RSpec.describe Hash do 
    subject(:boo) do 
        {a1: 1, b1: 3}
    end

    # let(:boo) {  {a1: 1, b1: 3, c1: 4} } 

    it 'has two key-value pairs' do
        expect(subject.length).to eq(2)
        expect(boo.length).to eq(2)
    end

    describe 'nested example' do
        it 'has two key-value pairs' do
            expect(subject.length).to eq(2)
            expect(boo.length).to eq(2)
        end
    end
end