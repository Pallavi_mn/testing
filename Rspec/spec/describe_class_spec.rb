# gives reference to the class under test. (describe calss is dynamic)
class Royal
  attr_reader :name

  def initialize(name)
    @name = name
  end
end

RSpec.describe Royal do
  # subject { King.new('Raj') } #if we change the class name also..it will break our testing here
  # let(:louis) { King.new('Louis')}

  subject { described_class.new('Raj') }  #if we change the class name also..it won't break our testing
  let(:louis) { described_class.new('Louis') }

  it 'represent a great person' do
    expect(subject.name).to eq('Raj')
    expect(louis.name).to eq('Louis')
  end
end
