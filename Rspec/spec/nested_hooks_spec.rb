RSpec.describe 'nested hooks' do
    before(:context) do
        puts "Outer Before Context"
    end

    before(:example) do
        puts "Outer Before Example"
    end

    it 'does basic math' do
        expect(1 + 1).to eq(2)
    end

    context 'with condition A' do
        before(:context) do
            puts "INNER Before Context"
        end
    
        before(:example) do
            puts "INNER Before Example"
        end

        it 'does bastic math' do
            expect(1 + 1).to eq(2)
        end
    end
end
