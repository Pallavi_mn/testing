Given('I visit the URL') do
  @browser = Watir::Browser.new(:chrome)
  @browser.goto 'www.google.com'
end

When('The page loads') do
  expect(@browser.title).to eq 'Google'
end

Then('I should see the homepage') do
  puts @browser.image.src
end

When('I search a keyword {string}') do |keyword|
  @browser.input(name: 'q').set keyword
  @browser.button.click
end

Then('I should see some search results') do
  @browser.close
end
